//
//  MobileConsents.h
//  MobileConsentsSDK
//
//  Created by Jan Lipmann on 21/09/2020.
//  Copyright © 2020 Droids on Roids. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for MobileConsents.
FOUNDATION_EXPORT double MobileConsentsVersionNumber;

//! Project version string for MobileConsents.
FOUNDATION_EXPORT const unsigned char MobileConsentsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MobileConsents/PublicHeader.h>


