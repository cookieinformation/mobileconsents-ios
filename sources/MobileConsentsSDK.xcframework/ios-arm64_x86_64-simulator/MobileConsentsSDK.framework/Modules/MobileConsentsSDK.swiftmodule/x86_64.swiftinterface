// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.3 (swiftlang-1200.0.29.2 clang-1200.0.30.1)
// swift-module-flags: -target x86_64-apple-ios12.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name MobileConsentsSDK
import Foundation
import Swift
import UIKit
public typealias Parameters = [Swift.String : Any]
public enum NetworkError : Swift.String, Swift.Error {
  case parametersMissing
  case encodingFailed
  case urlMissing
  public typealias RawValue = Swift.String
  public init?(rawValue: Swift.String)
  public var rawValue: Swift.String {
    get
  }
}
public enum ParameterEncoding {
  case urlEncoding
  case jsonEncoding
  case urlAndJsonEncoding
  public func encode(urlRequest: inout Foundation.URLRequest, bodyParameters: MobileConsentsSDK.Parameters?, urlParameters: MobileConsentsSDK.Parameters?) throws
  public static func == (a: MobileConsentsSDK.ParameterEncoding, b: MobileConsentsSDK.ParameterEncoding) -> Swift.Bool
  public var hashValue: Swift.Int {
    get
  }
  public func hash(into hasher: inout Swift.Hasher)
}
public struct ProcessingPurpose : Swift.Codable {
  public let consentItemId: Swift.String
  public let consentGiven: Swift.Bool
  public let language: Swift.String
  public init(consentItemId: Swift.String, consentGiven: Swift.Bool, language: Swift.String)
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
public struct ConsentSolution : Swift.Codable {
  public let id: Swift.String
  public let versionId: Swift.String
  public let consentItems: [MobileConsentsSDK.ConsentItem]
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
public struct Consent {
  public let consentSolutionId: Swift.String
  public let consentSolutionVersionId: Swift.String
  public let timestamp: Foundation.Date
  public var processingPurposes: [MobileConsentsSDK.ProcessingPurpose]
  public let customData: [Swift.String : Swift.String]?
  public init(consentSolutionId: Swift.String, consentSolutionVersionId: Swift.String, customData: [Swift.String : Swift.String]? = [:])
  public mutating func addProcessingPurpose(_ purpose: MobileConsentsSDK.ProcessingPurpose)
  public func JSONRepresentation() -> [Swift.String : Any]
}
@_hasMissingDesignatedInitializers final public class MobileConsents {
  public typealias ConsentSolutionCompletion = (Swift.Result<MobileConsentsSDK.ConsentSolution, Swift.Error>) -> Swift.Void
  convenience public init(withBaseURL url: Foundation.URL)
  final public func fetchConsentSolution(forUniversalConsentSolutionId universalConsentSolutionId: Swift.String, completion: @escaping MobileConsentsSDK.MobileConsents.ConsentSolutionCompletion)
  final public func postConsent(_ consent: MobileConsentsSDK.Consent, completion: @escaping (Swift.Error?) -> Swift.Void)
  final public func getSavedConsents() -> [MobileConsentsSDK.SavedConsent]
  final public func cancel()
  @objc deinit
}
public struct ConsentTranslation : Swift.Codable {
  public let language: Swift.String
  public let shortText: Swift.String
  public let longText: Swift.String
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
public struct ConsentItem : Swift.Codable {
  public let id: Swift.String
  public let translations: [MobileConsentsSDK.ConsentTranslation]
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
public typealias NetworkProviderCompletion = (Foundation.Data?, Foundation.URLResponse?, Swift.Error?) -> ()
public struct JSONParameterEncoder {
  public func encode(urlRequest: inout Foundation.URLRequest, with parameters: MobileConsentsSDK.Parameters) throws
}
public struct URLParameterEncoder {
  public func encode(urlRequest: inout Foundation.URLRequest, with parameters: MobileConsentsSDK.Parameters) throws
}
public struct SavedConsent {
  public let consentItemId: Swift.String
  public let consentGiven: Swift.Bool
}
extension MobileConsentsSDK.NetworkError : Swift.Equatable {}
extension MobileConsentsSDK.NetworkError : Swift.Hashable {}
extension MobileConsentsSDK.NetworkError : Swift.RawRepresentable {}
extension MobileConsentsSDK.ParameterEncoding : Swift.Equatable {}
extension MobileConsentsSDK.ParameterEncoding : Swift.Hashable {}
